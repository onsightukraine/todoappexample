//
//  DefaultsStoreService.swift
//  TodoListApp
//
//  Created by Nikolay Chaban on 3/8/19.
//  Copyright © 2019 Nikolay Chaban. All rights reserved.
//

import Foundation

class DefaultsStoreService {
  
  let userDefaults = UserDefaults.standard
  
  class var sharedInstance: DefaultsStoreService {
    struct Static {
      static let shared: DefaultsStoreService = DefaultsStoreService()
    }
    return Static.shared
  }
  
  func saveToken(_ token: String) {
    userDefaults.set(token, forKey: "TokenKey")
    saveChanges()
  }
  
  func obtainToken() -> String? {
    return userDefaults.value(forKey: "TokenKey") as? String
  }
  
  func removeToken() {
    userDefaults.removeObject(forKey: "TokenKey")
  }
  
  private func saveChanges() {
    userDefaults.synchronize()
  }
}
