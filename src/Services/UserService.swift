//
//  UserService.swift
//  TodoListApp
//
//  Created by Nikolay Chaban on 3/6/19.
//  Copyright © 2019 Nikolay Chaban. All rights reserved.
//

import Foundation

typealias UserResponseBlock = (_ response: UserModel?, _ error: ErrorViewModel?) -> ()

protocol UserService {
    func obtainUserInfo(completion: @escaping(UserResponseBlock))
    func checkIfUserLoggedIn() -> Bool
    func registerUserWithDetails(with requestInfo: Register.UserDetails.Request, completion: @escaping(UserResponseBlock))
    func loginUserWithDetails(with username: String, password: String, completion: @escaping(UserResponseBlock))
}

protocol UserServiceOutput {
    func didObtainedUserDetail(userDetail: UserModel)
}

class UserServiceImplementation: UserService {
    
    func loginUserWithDetails(with username: String, password: String, completion: @escaping (UserResponseBlock)) {
      
      if ( username == defaultUserName && password == defaultUserPwd ) {
        let userModel = UserModel(fullName: "User Name", userName: "Admin", email: "admin@domain.com", password: "admin", userPicture: nil, token: "kajfijaweifjaskldfjaiwejfaiijdfsd")
        DefaultsStoreService.sharedInstance.saveToken(userModel.token!)
        completion(userModel, nil)
      } else {
        
        let errorModel = ErrorViewModel(title: "Login error",
                                        message: "Entered incorrect data",
                                        buttonTitles: ["Cancel", "Confirm"],
                                        serverCode: .wrongDataError)
        completion(nil, errorModel)
      }
    }
    
    func obtainUserInfo(completion: @escaping (UserResponseBlock)) {
        
    }
    
    func checkIfUserLoggedIn() -> Bool {
        guard let _ = DefaultsStoreService.sharedInstance.obtainToken() else { return false }
        
        return true
    }
    
    func registerUserWithDetails(with requestInfo: Register.UserDetails.Request, completion: @escaping (UserResponseBlock)) {
      let userModel = UserModel(fullName: requestInfo.fullName, userName: requestInfo.userName, email: requestInfo.email!, password: requestInfo.password!, userPicture: "DefaultAvatar", token: "kasdkfjakdfjaksjdfkadjfsdf")
      DefaultsStoreService.sharedInstance.saveToken(userModel.token!)
        completion(userModel, nil)
    }
}
