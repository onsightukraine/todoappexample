//
//  StringExt.swift
//  SwiftTipsProject
//
//  Created by OnSightTeam on 1/14/19.
//  Copyright © 2019 Nikolay Chaban. All rights reserved.
//

import Foundation
import UIKit
import CoreLocation

extension String {
    
    //MARK: show str without HTML tags
    var withoutHtmlTags: String {
        return self.replacingOccurrences(of: "<[^>]+>", with: "", options: .regularExpression, range: nil)
    }
    
    //MARK: show str without Whitespaces And Newlines
    var removingWhitespacesAndNewlines: String {
        return components(separatedBy: NSCharacterSet.newlines).joined()
    }
    
    //MARK: check valid Email
    static func isValidEmail(_ checkStr: String) -> Bool {
        let stricterFilterString = "[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}"
        let emailRegexp = stricterFilterString ;
        return NSPredicate(format: "SELF MATCHES %@", emailRegexp).evaluate(with: checkStr)
    }
    
    //MARK: check valid Username
    static func isValidUsername(_ username : String) -> Bool {
        if username.count > 5 && username.count < 16{
            let regex =  "^[a-z]([a-z0-9]*[-_][a-z0-9][a-z0-9]*)$"
            
            let passwordTest = NSPredicate(format: "SELF MATCHES %@", regex)
            return passwordTest.evaluate(with: username)
        } else {
            return false
            
        }
    }
    
    //MARK: check valid Password
    func isValidPassword(testStr:String?) -> Bool {
        guard testStr != nil else { return false }
        
        // at least one uppercase,
        // at least one digit
        // at least one lowercase
        // 8 characters total
        let passwordTest = NSPredicate(format: "SELF MATCHES %@", "(?=.*[A-Z])(?=.*[0-9])(?=.*[a-z]).{6,}")
        return passwordTest.evaluate(with: testStr)
    }
    
    //MARK: convert Base64String To Image
    func convertBase64StringToImage() -> UIImage? {
        
        let imageData = Data.init(base64Encoded: self, options: .init(rawValue: 0))
        
        if let imgData = imageData {
            let image = UIImage(data: imgData)
            
            return image
        }
        return nil
    }
    
    //MARK: convert Adress To Coordinate
    func convertAdressToCoordinate(adress: String) -> CLLocationCoordinate2D? {
        
        var currentcoordinate: CLLocationCoordinate2D?
        
        let geoCoder = CLGeocoder()
        geoCoder.geocodeAddressString(self) { (placemarks, error) in
            
            let geocoder = CLGeocoder()
            geocoder.geocodeAddressString(self, completionHandler: {(placemarks, error) -> Void in
                if((error) != nil){
                    print("Error", error ?? "")
                }
                if let currentlocation = placemarks?.first?.location {
                    
                    currentcoordinate = currentlocation.coordinate
                    
                    
                }
            })
        }
        
        return currentcoordinate
    }
}


