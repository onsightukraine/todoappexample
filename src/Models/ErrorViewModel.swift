//
//  ErrorViewModel.swift
//  TodoListApp
//
//  Created by Nikolay Chaban on 3/6/19.
//  Copyright © 2019 Nikolay Chaban. All rights reserved.
//

import Foundation

struct ErrorViewModel {
  
  let title: String?
  let message: String?
  let buttonTitles: [String]?
  let serverCode: LoginErrorCodes
}

enum LoginErrorCodes: Int {
  case emptyUserNameError = 1
  case emptyPasswordError = 2
  case wrongDataError     = 3
  case emptyEmailError    = 4
  case undefineError      = 0
}
