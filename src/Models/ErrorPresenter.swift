//
//  ErrorPresenter.swift
//  TodoListApp
//
//  Created by Nikolay Chaban on 3/6/19.
//  Copyright © 2019 Nikolay Chaban. All rights reserved.
//

import UIKit

protocol ErrorPresenter {

  
  /// Method for presenting alert view with error details
  ///
  /// - Parameter viewModel: model object with occured error detail
  func presentError(viewModel: ErrorViewModel)
}

/// Extension of _ErrorPresenter_ protocol for common view controller error handling
extension ErrorPresenter where Self: UIViewController {
  
  /// Presents an error for a view controller using an alert
  ///
  /// - parameter viewModel: The view model for the error
  func presentError(viewModel: ErrorViewModel) {
    
    let alertController = UIAlertController(title: viewModel.title, message: viewModel.message, preferredStyle: .alert)
    
    if let buttonTitles = viewModel.buttonTitles {
      
      for title in buttonTitles {
        
        let action = UIAlertAction(title: title, style: UIAlertAction.Style.default, handler: nil)
        alertController.addAction(action)
      }
    }
    
    present(alertController, animated: true, completion: nil)
  }
}
