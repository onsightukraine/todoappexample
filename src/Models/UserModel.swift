//
//  RegisterUserModel.swift
//  TodoListApp
//
//  Created by Nikolay Chaban on 3/6/19.
//  Copyright © 2019 Nikolay Chaban. All rights reserved.
//

import Foundation

struct UserModel {
  let fullName: String?
  let userName: String?
  let email: String
  let password: String
  let userPicture: String?
  let token: String?
}
