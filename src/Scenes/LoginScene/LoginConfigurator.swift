//
//  LoginConfigurator.swift
//  TodoListApp
//
//  Created by Nikolay Chaban on 3/6/19.
//  Copyright © 2019 Nikolay Chaban. All rights reserved.
//

import UIKit

class LoginConfigurator {
  
  class var sharedInstance: LoginConfigurator {
    struct Static {
      static let sharedInstance: LoginConfigurator = LoginConfigurator()
    }
    
    return Static.sharedInstance
  }
  
  func configuration(viewController: LoginViewController) {
    let presenter = LoginPresenter()
    presenter.viewController = viewController
    
    let interactor = LoginInteractor()
    interactor.presenter = presenter
    
    let router = LoginRouter()
    router.viewController = viewController
    router.dataStore = interactor
    
    viewController.interactor = interactor
    viewController.router = router
  }
  
}
