//
//  ItemsListConfigurator.swift
//  TodoListApp
//
//  Created by Nikolay Chaban on 3/6/19.
//  Copyright © 2019 Nikolay Chaban. All rights reserved.
//

import UIKit
import Foundation

class ItemsListConfigurator {
  
  class var sharedInstance: ItemsListConfigurator {
    struct Static {
      static let sharedInstance: ItemsListConfigurator! = ItemsListConfigurator()
    }
    return Static.sharedInstance
  }
  
  func configure(viewController: ItemsListViewController) {
    let presenter = ItemsListPresenter()
    presenter.viewController = viewController
    
    let interactor = ItemsListInteractor()
    interactor.presenter = presenter
    
    let router = ItemsListRouter()
    router.viewController = viewController
    router.dataStore = interactor
    
    viewController.interactor = interactor
    viewController.router = router
  }
}
