//
//  RegisterConfigurator.swift
//  TodoListApp
//
//  Created by Nikolay Chaban on 3/7/19.
//  Copyright © 2019 Nikolay Chaban. All rights reserved.
//

import Foundation

class RegisterConfigurator {
  
  class var sharedInstance: RegisterConfigurator {
    struct Static {
      static let sharedInstance: RegisterConfigurator = RegisterConfigurator()
    }
    
    return Static.sharedInstance
  }
  
  func configuration(viewController: RegisterViewController) {
    
    let presenter = RegisterPresenter()
    presenter.viewController = viewController
    
    let interactor = RegisterInteractor()
    interactor.presenter = presenter
    
    let router = RegisterRouter()
    router.viewController = viewController
    router.dataStore = interactor
    
    viewController.interactor = interactor
    viewController.router = router
  }
}
