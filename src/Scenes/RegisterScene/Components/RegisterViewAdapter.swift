//
//  RegisterViewAdapter.swift
//  TodoListApp
//
//  Created by Nikolay Chaban on 3/7/19.
//  Copyright © 2019 Nikolay Chaban. All rights reserved.
//

import UIKit

class RegisterScreenAdapterImplementation: NSObject {
  
  let underlineColor: UIColor = UIColor(red: 0.42, green: 0.8, blue: 0.96, alpha: 1.0)
  var errorType: LoginErrorCodes?
  
  //MARK: - Outlets -
  @IBOutlet weak var fullNameLabel: UILabel!
  @IBOutlet weak var fullNameUnderline: UIView!
  
  @IBOutlet weak var userNameLabel: UILabel!
  @IBOutlet weak var userNameUnderline: UIView!
  @IBOutlet weak var userNameErrorLabel: UILabel!
  
  @IBOutlet weak var emailLabel: UILabel!
  @IBOutlet weak var emailUnderline: UIView!
  @IBOutlet weak var emailErrorLabel: UILabel!
  
  @IBOutlet weak var passwordLabel: UILabel!
  @IBOutlet weak var passwordUnderline: UIView!
  @IBOutlet weak var passwordErrorLabel: UILabel!
  
  @IBOutlet weak var registerBtn: UIButton!
  
  func errorHighlightEntireWithType(_ error: ErrorViewModel) {
    errorType = error.serverCode
    
    switch error.serverCode {
    case .emptyUserNameError:
      userNameLabel.textColor           = .red
      userNameUnderline.backgroundColor = .red
      userNameErrorLabel.isHidden       = false
      userNameErrorLabel.text           = error.message
    case .emptyPasswordError:
      passwordLabel.textColor           = .red
      passwordUnderline.backgroundColor = .red
      passwordErrorLabel.isHidden       = false
      passwordErrorLabel.text           = error.message
    case .emptyEmailError:
      emailLabel.textColor           = .red
      emailUnderline.backgroundColor = .red
      emailErrorLabel.isHidden       = false
      emailErrorLabel.text           = error.message
    default: break
    }
    
    registerBtn.wiggleAnimation()
  }
  
}

extension RegisterScreenAdapterImplementation: UITextFieldDelegate {
  
  //MARK: - Textfield delegate methods -
  func textFieldDidBeginEditing(_ textField: UITextField) {
    resetEntire()
  }
  
  //MARK: - Internal methods -
  func resetEntire() {
    
    switch errorType {
    case .emptyUserNameError?:
      userNameLabel.textColor           = .white
      userNameUnderline.backgroundColor = underlineColor
      userNameErrorLabel.isHidden       = true
    case .emptyPasswordError?:
      passwordLabel.textColor           = .white
      passwordUnderline.backgroundColor = underlineColor
      passwordErrorLabel.isHidden       = true
    case .emptyEmailError?:
      emailLabel.textColor           = .white
      emailUnderline.backgroundColor = underlineColor
      emailErrorLabel.isHidden       = true
    default: break
    }
  }
}

